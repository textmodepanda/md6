# MD6 #

Master branch of this repository contains reference MD6 implementation from https://groups.csail.mit.edu/cis/md6/downloads.html. Some modifications were made over original code:
* Resolved issue with inline tick() which prevented code from building
* Added Makefile

Branch `md6chain` contains some demo for seeking collisions in 32bit MD6 hashes space. `md6chain` does many iterations like MD6(MD6(...,32),32) and seeks hash image using previous iteration hash as pre-image. When collision occurs program finds cycle length.

### How do I get set up? ###

Execute `make` and run binaries from code directory.