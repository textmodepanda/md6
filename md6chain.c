#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <assert.h>
#include <search.h>

#include "md6.h"

#define W 32              /* digest lenght constant for images and preimages */

int d;                    /* digest length */
int L;                    /* mode parameter */
int r;                    /* number of rounds */
int use_default_r;        /* 1 if r should be set to the default, 0 if r is explicitly provided */
unsigned char K[100];     /* key */
int keylen;               /* key length in bytes (at most 64) */
md6_state st;             /* md6 computation state */

int hash_comp(const void *a, const void *b) {
    if (a == b) {
        return 0;
    } else {
        if (a>b) {
            return 1;
        } else {
            return -1;
        }
    }
}

void noop_destroyer(void *node) {}

void init_defaults() {
    /* set default md6 parameter settings */
    d = W;                
    keylen = 0;
    L = 64;
    r = md6_default_r(d,keylen);
    use_default_r = 1;
}

void md6_step(char *preimage, md6_state *st) {
    assert(md6_full_init(st,d,K,keylen,L,r)==0);
    assert(md6_update(st, preimage, W)==0);
    assert(md6_final(st, NULL)==0);
}

int main(int argc, char* argv[]) {
    unsigned char last_preimage[] = "cafe";
    FILE *logfile = fopen("md6chain.log", "w");
    assert(logfile);
    init_defaults();
    void *root = NULL;
    uint32_t first_collision;
    size_t first_col_idx = 0;

    size_t i;
    for (i=0; i<=0x100000000; i++) {
        if ( ! (i % 1000000) ) {
            printf("\ri=%lu", i);
            fflush(stdout);

        }
        assert(tsearch( (void*)(*( (uint32_t *)last_preimage)), &root, hash_comp));

        md6_step(last_preimage, &st);
        fprintf(logfile, "%s\n",st.hexhashval);

        if ( tfind( (void*)(*( (uint32_t *)st.hashval)), &root, hash_comp) ) {
            if (!first_col_idx) {
                printf("\ndeja vu: i=%lu preimage %.2hhx%.2hhx%.2hhx%.2hhx => image %s, which is seen before\n", i, last_preimage[0], last_preimage[1], last_preimage[2], last_preimage[3], st.hexhashval);
                first_collision = *( (uint32_t *)st.hashval);
                first_col_idx = i;
            } else {
                if (first_collision == *( (uint32_t *)st.hashval) ) {
                    printf("\nHash loop collapsed in %lu cycles: preimage %.2hhx%.2hhx%.2hhx%.2hhx => image %s.\n", i - first_col_idx, last_preimage[0], last_preimage[1], last_preimage[2], last_preimage[3], st.hexhashval);
                    return 0;
                }
            }
        }
        *( (uint32_t *)last_preimage) = *( (uint32_t *)st.hashval);
    }

    tdestroy(root, noop_destroyer);
    return 0;
}
