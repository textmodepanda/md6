CC_OPTS = -O2 -march=native
md6_objects = md6_nist.o md6_mode.o md6_compress.o

all: md6sum md6chain

md6sum: md6sum.c $(md6_objects) 
	gcc $(CC_OPTS) -o $@ $^

md6chain: md6chain.c $(md6_objects)
	gcc $(CC_OPTS) -o $@ $^

md6_nist.o: md6_nist.c
	gcc $(CC_OPTS) -o $@ -c $^

md6_mode.o: md6_mode.c
	gcc $(CC_OPTS) -o $@ -c $^

md6_compress.o: md6_compress.c
	gcc $(CC_OPTS) -o $@ -c $^

clean:
	rm -f md6sum md6chain $(md6_objects)
